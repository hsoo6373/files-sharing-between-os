import java.util.Random


fun main(args : Array<String>) {
    println("Hello, World!")

    //semi-colon is optional

    val name = "val is an immutable type";

    var nameTwo = "var is mutable"

    var bigDouble: Double = Double.MAX_VALUE   //maximum value for double stored in the variable bigDouble
    var smallInt: Int = Int.MIN_VALUE  //minimum value for integer stored in the variable smallInt

    println("Biggest Double : " + bigDouble)
    println("Smallest Int : $smallInt")     // this is string interpolation (using '$' and the variable name within
                                            // the double quote to call the variable without using "+"
    var dblNum1: Double = 1.111111111111111111
    var dblNum2: Double = 1.111111111111111111
    // floating point numbers are not precise
    println("Sum : " + (dblNum1 + dblNum2))


    // Boolean

    // 'is' is used to see if the value is an integer/float/boolean/long
    if (true is Boolean){
        print("true is boolean\n")
    }
	if (1 is Int){
		print("Yes, 1 is an integer")
	}

    // Char

    var letterGrade: Char = 'A'

    println ("A is a Char : ${letterGrade is Char}")  //another way of using string interpolation
                                                      //this prints "A is a char : true"

    // Casting

    println("3.14 to Int : " + (3.14.toInt()))  // converting double to integer
    println("A to Int : " + ('A'.toInt()))      // Gives the ASCII number of A
    println("65 to Char : " + 65.toChar())

    //String

    val myName = "Jacob Sood"   //cannot be changed
    val longStr =  """This is a long string"""  //cannot be changed

    var fName: String = "Doug"  // can be changed (as seen in line 52)
    var lName: String = "Smith"  // can be changed

    fName = "Sally"

    var fullName = "$fName $lName"
    println("Name : $fullName")

    println("1 + 2 = ${1 + 2}") //arithmetic calculation within the string

    println("String Length : ${longStr.length}")  // no need to put parentheses at the end to use the length method

    // comparing strings

    var str1 = "A random string"
    var str2 = "a random string"

    println("String Equal : ${str1.equals(str2)}")  //gives boolean true/false

    println("Compare A to B : ${"A".compareTo("B")}")  //can use double quotes within double quotes without having to escape the character using \

    // getting char at certain index

    println("2nd Index : ${str1[2]}")  // instead of using .charAt(number), just type the number inside the square brackets

    //substring

    println("Index 2 - 7 : ${str1.subSequence(2,8)}")   //using .subSequence(2,8) instead of .substring(2,8)  (recall from java - index 8 is not included)

    //contains

    println("Contains random : ${str1.contains("random")}")  //prints true if str1 contains "random"

    //Arrays

    var myArray = arrayOf(1, 1.23, "Doug")  //not strict on what data type each array can hold

    println(myArray[2]) //prints the third element in the array

    // array length

    println("Array size : ${myArray.size}")  // use .size instead of .length

    //array contains...
    println("Doug in Array : ${myArray.contains("Doug")}") // prints true if 'Doug' is in the array. Much quicker and easier than for loops!

    //copy elements from 1 array to another
    var partArray = myArray.copyOfRange(0,1) // this copies the first element (element 0) from myArray into partArray (1 is not included)
    println("size of PartArray : ${partArray.size}")  //now contains one element
    println("it contains : ${partArray[0]}" )

    println("First : ${myArray.first()}")  //prints the element at index 0
    println("Last : ${myArray.last()}") //prints the element at the last index
    println("Doug Index : ${myArray.indexOf("Doug")}")  //prints the index number of Doug

    //more String methods

    println("First : ${str1.first()}")  // prints the first character in the string
    println("Last : ${str1.last()}") //prints the last character in the string

    println("Random Index : ${str1.indexOf("random")}") //prints the starting index of the word


    var sqArray = Array(5, { x -> x * x})   //array of length 5  // each index is multiplied by itself
    println(sqArray[2])   // prints 4

    //type specific arrays
    var arr2: Array<Int> = arrayOf(1,2,3) //will throw an error if the array does not contain an integer


    //ranges - define starting and ending value - lists

    val oneTo10 = 1..10  //this variable contains numbers 1 to 10

    val alpha = "A".."Z" //contains letter A to Z

    println("R in Alpha : ${"R" in alpha}")  //true if R is in alpha

    val tenTo1 = 10.downTo(1) // decrement from 10 to 1 //note 10..1 will not work
    val ten1 = 10..1 // DO NOT TRY THIS

    val twoTo20 = 2..20 // or
    val anotherTwoTo20 = 2.rangeTo(20) //This is preferred

    println("${20 in twoTo20} ${20 in anotherTwoTo20}") //true true

    val rng3 = oneTo10.step(3) // starts from 1 and then starts incrementing by 3..  1,4,7,10 ... cannot go past 10 in this case
    println("${7 in rng3}") // true
    println("${13 in rng3}") // false
    //proof here

    for(x in rng3) println("rng3 : $x") // prints 1, 4, 7, 10  // for loop will be covered later

    for(x in tenTo1.reversed()) println("Reverse : $x") //reverse method

    val reverseThis = "Hello"
    println("${reverseThis.reversed()}")  //reverses Hello to "olleH"

    //conditional operators

    val age = 8

    if(age < 5) {
        println("too young")
    } else if (age == 5) {
        println("go to kindergarten")
    } else if ((age > 5) && (age < 17)) {
        val grade = age - 5;
        println("go to grade $grade")
    } else {
        println("Go to university")
    }

    //when (similar to switch)
    when(age){
        0,1,2,3,4 -> println("Go to preschool")
        5 -> println("go to kindergarten")
        in 6..16 -> {  //using range
            val grade = age - 5
            println("go to grade $grade")
        }
        else -> println("Go to university")
    }

    //looping

    for(x in 1..10) { //cycle through 1 through 10
        println("Loop : $x")  //prints loop : 1, loop : 2, ... , loop : 10
    }

    //random number... import java.util.Random

    val rand = Random()
    val number = rand.nextInt(50) + 1

    var guess = 0

    while(number != guess){
        guess += 1
    }
    println("number was $guess")


    //break and continue
    for(x in 1..20){ //loop 20 times
        if (x % 2 == 0){
            continue
        }
        println("Odd : $x")

        if (x == 15){   // or simply: if (x == 15) break
            break
        }
    }

    var arr3: Array<Int> = arrayOf(3, 6, 9)

    for(i in arr3.indices){
        println("Mult 3 : ${arr3[i]}")
    }

    for((index, value) in arr3.withIndex()) {
        println("Index : $index Value : $value")  //prints out the value with its index
    }


    //functions start with 'fun'

    fun add(num1: Int, num2: Int) : Int = num1 + num2   // this function takes in 2 parameters which are both of type Int
                                                        // the return type is Int
    println("5 + 4 = ${add(5,4)}")                      // return type is not needed for single line function
                                                        // this example is a single line function

    //e.g. function without return type

    fun subtract(num1: Int, num2: Int) = num1 - num2
    println("5 - 4 = ${subtract(5,4)}")

    // function without a return value
    fun sayHello(name : String) : Unit = println("Hello $name")  //in kotlin we use 'Unit' instead of 'void'

    sayHello("Paul")

    //calling the function outside main... see the 'nextTwo' function at the end of this main function
    val(second, third) = nextTwo(1)  //1 argument given to the function, two parameters received
    println("1 $second $third ")

    var sum = getSum(3,3,10,22,493,21)
    println("the sum is : $sum")
    println("another sum is ${getSum(1,2,3,4,5)}")

    val multiply = {num1: Int, num2: Int -> num1 * num2}
    println("5 * 3 = ${multiply(5,3)}")

    print("5! ${fact(5)}")  // using the function 'fact'




}

//function outside main

fun nextTwo(num: Int): Pair<Int, Int>{  //receive 1 number and return next 2 in line
    return Pair(num+1, num+2)
}

fun getSum(vararg nums: Int): Int{ //vararg is an array
    var sum = 0

    nums.forEach {n -> sum += n} //forEach is used to cycle through the array
    return sum
}

fun fact(x: Int): Int{
    tailrec fun facctTail(y: Int, z: Int): Int {
        if (y == 0) return z
        else return factTail(y - 1, y * z)
    }
    return factTail(x, 1)
}



// Source : https://www.youtube.com/watch?v=H_oGi8uuDpA
// collection operators, exception handling, lists, maps, classes, inheritance, interfaces, null safety can be found
// in the link above
