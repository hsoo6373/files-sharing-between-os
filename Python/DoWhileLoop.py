import random

n = random.randint(1,10)

while True:
    try:
        number = int(input("Guess a number between 1 and 10: "))
        if number == n:
            break
    except ValueError:
        print("Enter a number only")

print("You guessed it!")