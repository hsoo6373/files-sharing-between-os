# Have the user enter their investment amount and expected interest

investment = float(input("Enter your investment amount: "))
interest_rate = float(input("Enter the expected interest rate: ")) * .01

for i in range(10):
    investment += investment * interest_rate

print("Investment after 10 years : {:.2f}".format(investment))
# Each year their investment will increase by their investment + their investment * interest rate



# Print out the earning after a 10 year period


