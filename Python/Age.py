# We'll provide different output based on age
# 1 - 18 -> Important
# 21, 50, > 65 -> Important
# All other -> Not Important

# Receive age and store in age

age = eval(input("Enter age: "))

# if age is both greater than or equal to 1 and less than or equal to 18 important

if age >= 1 and age <= 18:
    important = True

# if age is either 21 or 50
elif age == 21 or age == 50:
    important = True

elif not(age < 65):
    important = True

else:
    important = False

if important:
    print("Important Birthday")
else:
    print("Not important birthday")