age = eval(input("Enter your age: "))
if (age < 5):
    print("Too young for school")
elif (age == 5):
    print("Go to Kindergarten")
elif (age > 5) and (age <= 17):
    print("Go to grade {}".format(age - 5))
else:
    print("Go to college")