﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arguments_and_method_overloading
{
    class Program
    {
        public static void Main(string[] args)
        {
            int x = 3;
            Func(ref x);
            Console.WriteLine(x);
            Func(x);
            Console.WriteLine(x);
            int y = 100;
            Func(y: y, x: x);
            Console.WriteLine(y + x);
            Func(out x, out y);
            Console.WriteLine("x is {0} and y is {1}", x, y);

        }

        public static void Func(ref int a)
        {
            a++;
        }
        public static void Func(int a)
        {
            a++;
        }
        public static void Func(int x, int y)
        {
            Console.WriteLine("X is {0} and y is {1}", x, y);
            x++;
            y++;
        }
        public static void Func(out int x, out int y)
        {
            x = 100;
            y = 50;
        }


    }
}
