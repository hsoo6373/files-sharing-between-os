﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace SoloLearn
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("This program will give your weight(N) on other planets and their moon/s!\nEnter your mass in kg: ");
            double mass = 0;

            try
            {
                mass = Convert.ToDouble(Console.ReadLine());
                Console.WriteLine(mass);
            }
            catch (Exception)
            {
                Console.WriteLine("Only real numbers are accepted. Try again later.");
                Environment.Exit(0);
            }

            Console.WriteLine();
            string[][] planets = new string[][]{
                new string[]  {"Earth", "Moon"},
                new string[]  {"Mercury"},
                new string[]  {"Venus"},
                new string[]  {"Mars", "Deimos", "Phobos"},
                new string[]  {"Dwarf Ceres"},
                new string[]  {"Jupiter", "Ganymede", "Callisto", "Io", "Europa", "Himalia", "Amalthea", "Thebe","Elara", "Metis", "Pasiphae", "Carme", "Sinope", "Lysithea", "Ananke", "Leda", "Themisto", "Callirrhoe", "Praxidike", "Megaclite", "Locaste", "Taygete", "Kalyke", "Autonoe", "Harpalyke", "Thyone", "Hermippe", "Chaldene", "Aoede", "Eukalade", "Isonoe", "Helike", "Carpo", "S/2003 J5","S/200 J11", "Aitne", "Eurydome", "Hegomone", "Arche", "Euanthe", "Sponde", "S/2003 J2", "S/2003 J9","Euporie", "Thelxinoe", "S/2003 J3", "S/2003 J18", "Erinome", "Pasithee", "Kore", "Cyllene", "Mneme", "Kale", "Kallichore", "S/2003 J16", "S/2003 J19", "S/2003 J15", "S/2003 J10", "S/2003 J23", "S/2011 J2", "S/2010 J1", "S/2003 J4", "S/2011 J1", "S/2010 J2"},
                new string[]  {"Saturn", "Titan", "Rhea", "Iapetus", "Dione", "Tethys", "Enceladus", "Mimas", "Hyperion", "Phoebe", "Janus", "Epimetheus", "Prometheus", "Pandora", "Siarnaq", "Helene", "Albiorix", "Atlas", "Pan", "Telesto", "Paaliaq", "Calypso", "Ymir", "Kiviuq", "Tarvos", "Ijiraq", "Erriapo", "Skathi", "Hyrrokkin", "Tarqeq", "Nariv", "Mundilfari", "Suttungr", "Thymr", "Bestla", "Kari", "Bergelmir", "Greip", "Jarnsaxa", "Skoll", "Benhionm", "Hati", "Aegir", "Surtur", "Loge", "Fornjot", "Farbauti", "Fenrir", "Methone", "Polydueces", "Pallene", "Aegeon", "S/2007 S2", "S/2004 S13", "S/2006 S1", "S/2004 S17", "S/2004 S12", "S/2007 S3", "S/2004 S7", "S/2006 S3"},
                new string[]  {"Uranus", "Titania", "Oberon", "Umbriel", "Ariel", "Miranda", "Sycorax", "Puck", "Portia", "Juliet", "Caliban", "Belinda", "Cressida", "Rosalind", "Desdemona", "Bianca", "Ophelia", "Cordelia", "Perdita", "Prospero", "Setebos", "Mab", "Stephano", "Cupid", "Francisco","Ferdinand", "Margeret", "Trinculo"},
                new string[]  {"Neptune", "Triton", "Proteus", "Nereid", "Larissa", "Galatea", "Despina", "Thalassa", "Naiad", "Halimede", "Neso", "Sao", "Laomedeia", "Psamathe", "S/2004 N1"},
                new string[]  {"Dwarf Pluto", "Charon", "Hydra", "Nix", "Kerberos", "Styx"},
                new string[]  {"Dwarf Orcus", "Vanth"},
                new string[]  {"Dwarf Haumea", "Hi'iaka", "Namaka"},
                new string[]  {"Dwarf Makemake"},
                new string[]  {"Dwarf Eris", "Dysnomia"},
                new string[]  {"Dwarf Sedna"}
            };

            double[][] gravitationalAttraction = new double[][]{
                new double[]  {9.807, 1.622},
                new double[]  {3.7},
                new double[]  {8.87},
                new double[]  {3.711, 0.003, 0.0057},
                new double[]  {0.27},
                new double[]  {24.79, 1.428, 1.236, 1.796, 1.315, 0.062, 0.020, 0.013, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                new double[]  {10.44, 1.352, 0.264, 0.223, 0.232, 0.145, 0.113, 0.064, 0.021, 0.048, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,0,0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                new double[]  {8.69, 0.367, 0.346, 0.2, 0.269, 0.079, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                new double[]  {11.15, 0.779, 0.07, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                new double[]  {0.62, 0.288, 0, 0, 0, 0},
                new double[]  {0.18, 0},
                new double[]  {0.44, 0, 0},
                new double[]  {0.5},
                new double[]  {0.827, 0},
                new double[]  {0.00014}
            };

            for (int i = 0; i < planets.Length; i++)
            {
                Console.WriteLine("Planets                  \tMoons");
                Console.Write("Weight on {0} = {1:0.00}N", planets[i][0], gravitationalAttraction[i][0] * mass);
                if (planets[i].Length == 1)
                    Console.WriteLine("\t{0} has no moons", planets[i]);
                else
                {
                    if (gravitationalAttraction[i][1] == 0)
                        Console.WriteLine("\tAcceleration Due to Gravity for {0} is not yet known.", planets[i][1]);
                    else
                        Console.WriteLine("\tWeight on {0} = {1:0.00}N", planets[i][1], gravitationalAttraction[i][1] * mass);
                }
                for (int x = 2; x < planets[i].Length; x++)
                {
                    if (gravitationalAttraction[i][x] == 0)
                        Console.WriteLine("                         \tAcceleration Due to Gravity for {0} is not yet known.", planets[i][x]);
                    else
                        Console.WriteLine("                         \tWeight on {0} = {1:0.00}N", planets[i][x], gravitationalAttraction[i][x] * mass);
                }
                Console.WriteLine("\n------------------------------------------------------------------------\n");
            }
        }
    }
}
