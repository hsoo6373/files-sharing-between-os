﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recursion
{
    class Program
    {
        static void Main(string[] args)
        {
            Name na = new Name();
            string thee = "hello";
            Console.WriteLine(thee = thee.Insert(5, " world"));
            Console.WriteLine(thee.Remove(5));
            Console.WriteLine(thee[1]);
        }


    }
    class Name
    {
        public Name()
        {
            Person p1 = new Person("Hrithvik", "Sood");
            Person p2 = new Person("Chaarvee", "Sood");
            Console.WriteLine("{0} {1}", p1.firstName, p1.lastName);
            Console.WriteLine("{0} {1}", p2.firstName, p2.lastName);
            p1.firstName = "Jacob";
            p1.lastName = "Curry";
            p2.firstName = "Anna";
            p2.lastName = "Max";
            Console.WriteLine("{0} {1}", p1.firstName, p1.lastName);
            Console.WriteLine("{0} {1}", p2.firstName, p2.lastName);
        }
    }
    class Person
    {
        string fname;
        string lname;
        public Person(string fname, string lname)
        {
            this.fname = fname;
            this.lname = lname;
        }
        public string firstName
        {
            get { return this.fname; }
            set { this.fname = value; }
        }
        public string lastName
        {
            get { return this.lname; }
            set { this.lname = value; }
        }
    }
}
