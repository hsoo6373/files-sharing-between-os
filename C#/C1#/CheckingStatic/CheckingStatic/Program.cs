﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckingStatic
{
    class Program
    {
        static void Main(string[] args)
        {
            Cat cat = new Cat();
            Console.WriteLine(Cat.stat);
            for (int i = 0; i < 10; i++)
                cat = new Cat();
        }
    }

    class Cat
    {
        public static int stat = 10;
        public int notStat = 10;

        public Cat()
        {
            stat--;
            notStat--;
            Console.WriteLine("{0} is static and {1} is not static", stat, notStat);
        }
    }
}
