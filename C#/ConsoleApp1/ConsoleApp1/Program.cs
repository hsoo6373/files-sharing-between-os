﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Person person = new Person(28);
            Console.WriteLine(person.Age);
            person.Age = 299;
            Console.WriteLine(person.Age);
            person = new Person(24);
            Console.WriteLine(person.Age);

        }
    }

    class Person
    {
        int age = 18;
        public int Age { get { return this.age; } set { this.age = value; } }
        public Person(int age)
        {
            Console.WriteLine("My age is {0}", this.age);
            this.age = age;
            Console.WriteLine("My age is now {0}", this.age); 
        }
    }
}
