﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Part_2
{
    class Program
    {
        static void Main(string[] args)
        {
            //var x = 15; //the compiler will automatically determine the type of x to be an int. 

            /* var y;
               y = 25; 
               This will cause an error since implicitly typed variables need to be initialised on 
               the same line as its declaration*/

            const string name = "Jacob";
            const double PI = 3.14;
            Console.WriteLine("{0} + {1}", PI, name);
            Console.WriteLine(name.Length);

            int x = 83;
            int y = 9;

            if (x > 9)
                Console.WriteLine("x is larger than y and it equals to {0}", x);
            else
                Console.WriteLine("Y is larger than x and it equals to {0}", y);
            Program cla = new Program();
            Console.WriteLine(cla.getValue(x));
            TowerofHanoi(4, 4, 4, 4);
            Console.WriteLine(-7 + 5);
        }

        int getValue (int x)
        {
            return x += 100;
        }
        public static void TowerofHanoi(int diskCount, int fromPole, int toPole, int viaPole)
        {
            if (diskCount == 1)
            {
                System.Console.WriteLine("Move disk from pole " + fromPole + " to pole " + toPole);
            }
            else
            {
                TowerofHanoi(diskCount - 1, fromPole, viaPole, toPole);
                TowerofHanoi(1, fromPole, toPole, viaPole);
                TowerofHanoi(diskCount - 1, viaPole, toPole, fromPole);
            }
        }
    }

}
