﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloWorld
{
	class Program
	{
		static void Main(string[] args)
		{
			Console.WriteLine("Hello World");
			Console.Write("Hello World!\n");

			int x = 145;
			Console.WriteLine(x);

			int y = 128;
			Console.WriteLine("x = {0}; y = {1}", x, y);
			Console.WriteLine("x + y = {0}", x + y);
			int converted = Convert.ToInt32(Console.ReadLine());
			Console.WriteLine(converted);
			bool ok = Convert.ToBoolean(Console.ReadLine());
			Console.WriteLine(ok);
			

		}
	}
}
